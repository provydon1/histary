<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="description" content="A leading multinational financial service platform for buying tokens based in Texas USA. Its secure and user friendly digital experience on Mobile and Internet makes it the preferred platform for buying tokens in the world">
        <title>Histary - A secure marketplace to buy history tokens"</title>
        <meta name="title" content="Welcome to Histary" />
        <meta name="keywords" content="HISTARY, TOKENS"/>
        <meta name="twitter:card" content="summary" />
        <meta property="og:description" content="A secure marketplace to buy history tokens" />
        <meta name="twitter:description" content="A secure marketplace to buy history tokens" />
        <meta property="og:image" content="{{ url('images/logo.png') }}" />
        <meta name="twitter:image" content="{{ url('images/logo.png') }}" />
        <meta property="og:type" content="website" />
        <meta property="og:site_name" content="Histary Tokens" />
        <meta property="og:locale" content="en_US" />

        {{-- <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous"> --}}
        <link href="{{ url('css/all.min.css') }}" rel='stylesheet' type='text/css'>
        <link href="{{ url('css/bootstrap.min.css') }}" rel='stylesheet' type='text/css'>
        <link href="{{ url('css/style.css') }}" rel='stylesheet' type='text/css'>
        <link href="{{ url('css/main.css') }}" rel='stylesheet' type='text/css'>
    </head>
    <body>
        @php
            // Session()->flush();
            // dd(Session()->all());
        @endphp
        <div class="container">
            <div class="row alert-row remove" align="center">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    <div class="row bg-dark" style="margin: 0;">
                        <i class="fas fa-times alert-x"></i>
                        <div class="alert alert-success col-md-12" role="alert">

                        </div>
                        <div class="alert alert-danger col-md-12" role="alert">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                        </div>
                        <div class="alert alert-info col-md-12" role="alert">

                        </div>
                    </div>
                </div>
                <div class="col-md-4"></div>
            </div>
        </div>
        <div id="app"></div>
        <script src="{{ asset('js/app.js') }}"></script>
        <script src="{{ asset('js/main.js') }}"></script>
        <script>
            $(".alert-x").click(function(event) {
                $('.alert-success').hide();
                $('.alert-info').hide();
                $('.alert-danger').hide();
                $('.alert-x').hide();
            });
        </script>
        @if ($errors->any())
            <script>
                $('.alert-danger').show();
                $('.alert-x').show();
            </script>
        @endif

        @if(session()->has('success'))
            <script>
                $('.alert-success').html("{{ session()->get('success') }}");
                $('.alert-success').show();
                $('.alert-x').show();
            </script>
        @endif

        @if(session()->has('fail'))
            <script>
                $('.alert-danger').html("{{ session()->get('fail') }}");
                $('.alert-danger').show();
                $('.alert-x').show();
            </script>
        @endif

        @if(session()->has('info'))
            <script>
                $('.alert-info').html("{{ session()->get('info') }}");
                $('.alert-info').show();
                $('.alert-x').show();
            </script>
        @endif
    </body>
</html>
