import Vue from 'vue'
import Vuex from 'vuex'

// Modules
import auth from './modules/auth'
import user from './modules/user'
import main from './modules/main'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {

    },
    mutations: {

    },
    actions: {

    },
    modules: {
        auth,
        user,
        main
    }
});
