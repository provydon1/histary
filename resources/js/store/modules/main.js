// State
const state = {
    coins: [],
    wallet: [],
    transactions: [],
    homeCoin: 'btc',
    summary: null,
    placeHolderCoin: [],
    newAddress: null,
    sendInfo: null
};

// Mutations
const mutations = {
    coins(state, data) {
        state.coins = data
    },
    wallet(state, data) {
        state.wallet = data
    },
    transactions(state, data) {
        state.transactions = data
    },
    homeCoin(state, data) {
        state.homeCoin = data
    },
    walletCoin(state, data) {
        state.walletCoin = data
    },
    summary(state, data) {
        state.summary = data
    },
    placeHolderCoin(state, data) {
        state.placeHolderCoin = data
    },
    newAddress(state, data) {
        state.newAddress = data
    },
    sendInfo(state, data) {
        state.sendInfo = data
    }
};

// Actions
const actions = {
    getCoins({ commit }) {
        return new Promise((resolve, reject) => {
            fetch('/data/get-coins')
                .then(res => res.json())
                .then(res => {
                    commit('coins', res.data)
                    resolve(true)
                })
                .catch(err => {
                    console.log(err)
                    reject(err)
                });
        })

    },
    getWallet({ commit }) {
        return new Promise((resolve, reject) => {
            fetch('/data/get-wallet')
                .then(res => res.json())
                .then(res => {
                    commit('wallet', res.data)
                    resolve(true)
                })
                .catch(err => {
                    console.log(err)
                    reject(err)
                });
        })

    },
    getTransactions({ commit }) {
        return new Promise((resolve, reject) => {
            fetch('/data/get-transactions')
                .then(res => res.json())
                .then(res => {
                    commit('transactions', res.data)
                    resolve(true)
                })
                .catch(err => {
                    console.log(err)
                    reject(err)
                });
        })

    },
    setHomeCoin({ commit }, data) {
        return new Promise((resolve, reject) => {
            commit('homeCoin', data)
            resolve(true)
        })
    },
    setWalletCoin({ commit }, data) {
        return new Promise((resolve, reject) => {
            commit('walletCoin', data)
            resolve(true)
        })
    },
    setSummary({ commit }, data) {
        return new Promise((resolve, reject) => {
            commit('summary', data)
            resolve(true)
        })
    },
    setPlaceHolderCoin({ commit }, data) {
        return new Promise((resolve, reject) => {
            commit('placeHolderCoin', data)
            resolve(true)
        })
    },
    generateNewAddress({ commit }, coin) {
        return new Promise((resolve, reject) => {
            fetch(`/data/generate-new-address/${coin}`)
                .then(res => res.json())
                .then(res => {
                    commit('newAddress', res)
                    resolve(true)
                })
                .catch(err => {
                    console.log(err)
                    reject(err)
                });
        })

    },
    sendInfo({ commit }, data) {
        return new Promise((resolve, reject) => {
            fetch('/data/sendInfo', {
                    method: 'post',
                    body: JSON.stringify(data),
                    headers: {
                        'content-type': 'application/json',
                        'Accept': 'application/json'
                    }
                })
                .then(res => res.json())
                .then(res => {
                    commit('sendInfo', res)
                    resolve(true)
                })
                .catch(err => {
                    console.log(err)
                    reject(err)
                });
        })

    }
};

// Getters
const getters = {
    coins: state => {
        return state.coins
    },
    wallet: state => {
        return state.wallet
    },
    transactions: state => {
        return state.transactions
    },
    walletCoin: state => {
        return state.walletCoin
    },
    homeCoin: state => {
        return state.homeCoin
    },
    summary: state => {
        return state.summary
    },
    placeHolderCoin: state => {
        return state.placeHolderCoin
    },
    newAddress: state => {
        return state.newAddress
    },
    sendInfo: state => {
        return state.sendInfo
    }
};


export default {
    state,
    mutations,
    actions,
    getters
}