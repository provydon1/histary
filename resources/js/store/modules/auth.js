// State
const state = {
    authenticated: false
};

// Mutations
const mutations = {
    authAuthenticated(state, authData) {
        state.authenticated = authData
    }
};

// Actions
const actions = {
    getAuthentication({ commit }) {
        return new Promise((resolve, reject) => {
            fetch('/auth/vueauth')
                .then(res => res.json())
                .then(res => {
                    commit('authAuthenticated', res.authenticated)
                    resolve(true)
                })
                .catch(err => {
                    console.log(err)
                    reject(err)
                });
        })
    }
};

// Getters
const getters = {
    authenticated: state => {
        return state.authenticated
    }
};


export default {
    state,
    mutations,
    actions,
    getters
}