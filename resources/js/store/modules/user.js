const state = {
    user: null
};
const mutations = {
    authUser(state, userData) {
        state.user = userData
    }
};
const actions = {
    getUser({ commit }) {
        fetch('/data/user')
            .then(res => res.json())
            .then(res => {
                commit('authUser', res)
            });
    }
};


const getters = {
    user: state => {
        return state.user;
    }
};


export default {
    state,
    mutations,
    actions,
    getters
}