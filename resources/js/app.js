require('./bootstrap');

import Vue from 'vue'

import store from './store'
import router from './router'
import App from './components/App'
import Progress from './progress'
import Scroll from './scroll'


const app = new Vue({
    el: '#app',
    store,
    router,
    Progress,
    components: { App },
    template: '<App/>'
})