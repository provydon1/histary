import Vue from 'vue'
import Router from 'vue-router'
import store from '../store'

// widgets
import Navbar from '../components/pagesWidgets/Navbar.vue'


// Authentication Pages
import Login from '../components/pages/Login.vue'
import Register from '../components/pages/Register.vue'


// Dashboard Components
import Dashboard from '../components/dashboard/Dashboard.vue'
import DashboardNavbar from '../components/widgets/Navbar.vue'
import home from '../components/dashboard/Home'
import BuyToken from '../components/dashboard/BuyToken.vue'

Vue.use(Router)

export default new Router({
    mode: 'history',
    base: '',
    routes: [{
            path: '/dashboard',
            component: Dashboard,
            children: [{
                    path: '/dashboard',
                    name: 'Home',
                    components: {
                        default: home,
                        'navbar-top': DashboardNavbar
                    }
                },
                {
                    path: '/dashboard/buy-token',
                    name: 'Buy Token',
                    components: {
                        default: BuyToken,
                        'navbar-top': DashboardNavbar
                    }
                },
            ]
        },
        {
            path: '/',
            redirect: '/login'
        },
        {
            path: '/login',
            name: 'Login',
            components: {
                default: Login,
                'navbar-side': Navbar
            }
        },
        {
            path: '/register',
            name: 'Register',
            components: {
                default: Register,
                'navbar-side': Navbar
            }
        },
        {
            path: '*',
            redirect: '/login'
        }
    ]
})