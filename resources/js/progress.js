import Vue from 'vue'
import VueProgressBar from 'vue-progressbar'

Vue.use(VueProgressBar, {
    color: 'rgba(0, 54, 99, 1)',
    failedColor: 'red',
    height: '2px'
})


export default VueProgressBar