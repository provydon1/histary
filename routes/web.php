<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Auth Routes
Route::group(['prefix'=>'auth'],function(){
    Route::post('/login', 'LoginController@authenticate');
    Route::post('/logout', 'LoginController@logout');
    Route::post('/register', 'RegisterController@createUser');
    Route::get('/vueauth', 'LoginController@vueAuth');
});

// Data Routes
Route::group(['prefix'=>'data'],function(){
    Route::get('/user', 'UserController@getUser');
});

Route::get('/dashboard/{any?}', function () {
    return view('vue');
})->middleware('vueauth');


Route::get('/{any?}', function () {
    return view('vue');
})->middleware('vueauth');

