<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class API extends Model
{
    //
    public function postRequest($data,$endpoint){
        $client = new Client([
            'timeout'  => 30.0,
            'headers' => [ 'Content-Type' => 'application/json']
        ]);
        try {
            // Call external API
            $response = $client->request('POST', 'https://api.swys.io/api/v1/user'.$endpoint, ['body' => $data]);

            // Check whether API call was successfull or not...
            $statusCode = $response->getStatusCode();
            if($statusCode == 200){
                // Api Request was succesfull
                $responseJSON = json_decode($response->getBody(), true);
                return $responseJSON;
            }
        } catch (GuzzleException $e) {
         $error_msg = 'Uh Ooo! ' . $e->getMessage();
         dd($error_msg);
         // return $e->getMessage();
         return $e->getCode();
        }
    }
}
