<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\API;
use App\User;
use Auth;

class UserController extends Controller
{
    //
    public function getUser(){
        return Auth::user();
    }
}
