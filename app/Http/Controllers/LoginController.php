<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class LoginController extends Controller
{
    //
    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            // Authentication passed...
            return redirect()->intended('dashboard');
        }
        else{
          return redirect()->back()->with(['fail'=>'Incorrect Email or Password']);
        }
    }

    public function logout(Request $request){
        $request->session()->flush();
        return redirect('/');
    }

    public function vueAuth(){
        if(Auth::check()){
            $data['authenticated'] = true;
            return $data;
        }else{
            $data['authenticated'] = false;
            return $data;
        }
    }
}
