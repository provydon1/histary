<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\User;
use Auth;



class RegisterController extends Controller
{
    public function createUser(Request $request)
    {
        $check = User::where('email',$request->email)->first();
        if($check == null){
            $user = new User;
            $user->first_name = $request->first_name;
            $user->last_name = $request->last_name;
            $user->email = $request->email;
            $user->password = Hash::make($request->password);
            $user->save();

            // login
            if (Auth::attempt(['email' => $request->email, 'password' => $request->password]))
            {
                return redirect()->intended('dashboard');
            }
            else
            {
                return redirect()->back()->with(['fail'=>'Error in Authentication']);
            }
        }else{
            return redirect()->back()->with(['fail'=>'Email already been used']);
        }
    }
}
