<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\API;
use Session;

class DataController extends Controller
{
    //
    public function getCoins(){
        $data = [];
        $api_data = json_encode($data);
        $api = new API;
        $responseJSON = $api->postRequest($api_data,'/coins');
        return $responseJSON;
    }

    public function getWallet(){
        $data = [
            "_token" => session('yourex_token')
        ];
        $api_data = json_encode($data);
        $api = new API;
        $responseJSON = $api->postRequest($api_data,'/wallet');
        return $responseJSON;
    }

    public function getTransactions(){
        $data = [
            "_token" => session('yourex_token')
        ];
        $api_data = json_encode($data);
        $api = new API;
        $responseJSON = $api->postRequest($api_data,'/transactions');
        return $responseJSON;
    }
    public function generateNewAddress($coin = null){
        if($coin != null){
            $data = [
                "_token" => session('yourex_token'),
                "coin" => $coin
            ];
            $api_data = json_encode($data);
            $api = new API;
            $responseJSON = $api->postRequest($api_data,'/new-address');
            return $responseJSON;
        }
    }

    public function sendInfo(Request $request){
    
        // dd($request->all());
        $data = $request->except('_token');
        $data['_token'] = session('yourex_token');
        // dd($data);
        $api_data = json_encode($data);
        $api = new API;
        $responseJSON = $api->postRequest($api_data,'/send-info');
        return $responseJSON;
    }

    public function send(Request $request){
        $data = $request->except('_token');
        $data['_token'] = session('yourex_token');
        // dd($data);
        $api_data = json_encode($data);
        $api = new API;
        $responseJSON = $api->postRequest($api_data,'/send');
        return $responseJSON;
    }

    public function updateProfile(Request $request){
        $data = $request->except('_token');
        $data['_token'] = session('yourex_token');
        // dd($data);
        $api_data = json_encode($data);
        $api = new API;
        $responseJSON = $api->postRequest($api_data,'/settings/update-profile');
        return $responseJSON;
    }

    public function prepareSMS(Request $request){
        $data = $request->except('_token');
        $data['_token'] = session('yourex_token');
        // dd($data);
        $api_data = json_encode($data);
        $api = new API;
        $responseJSON = $api->postRequest($api_data,'/settings/prepare-sms-code');
        return $responseJSON;
    }

    public function verifySMS(Request $request){
        $data = $request->except('_token');
        $data['_token'] = session('yourex_token');
        // dd($data);
        $api_data = json_encode($data);
        $api = new API;
        $responseJSON = $api->postRequest($api_data,'/settings/verify-sms');
        return $responseJSON;
    }

    public function changePassword(Request $request){
        $data = $request->except('_token');
        $data['_token'] = session('yourex_token');
        // dd($data);
        $api_data = json_encode($data);
        $api = new API;
        $responseJSON = $api->postRequest($api_data,'/settings/change-password');
        // dd($responseJSON);
        session(['yourex_token' => $responseJSON['data']['token']]);
        return $responseJSON;
    }

    public function google2FA(){
        $data['_token'] = session('yourex_token');
        $api_data = json_encode($data);
        $api = new API;
        $responseJSON = $api->postRequest($api_data,'/settings/googletwofa/prepare');
        return $responseJSON;
    }

    public function google2FAEnable(Request $request){
        $data = $request->except('_token');
        $data['_token'] = session('yourex_token');
        $api_data = json_encode($data);
        $api = new API;
        $responseJSON = $api->postRequest($api_data,'/settings/googletwofa/enable');
        return $responseJSON;
    }
}
