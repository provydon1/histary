<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class VueAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Get URL and check
        $url = $request->path();
        if($url == '/' || $url == 'login' || $url == 'register' || $url == 'forgot-password'){
            return $next($request);
        }else{
            if(Auth::check()){
                return $next($request);
            }else{
                return redirect('/login');
            }
        }
    }
}
